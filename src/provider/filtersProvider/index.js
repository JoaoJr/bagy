import { createContext, useContext, useState } from "react";

export const FiltersContext = createContext();

export const FiltersProvider = ({ children }) => {
  const [filterYear, setFilterYear] = useState("");
  const [filterMounth, setFilterMounth] = useState("");
  const [filterStore, setFilterStore] = useState("");

  return (
    <FiltersContext.Provider
      value={{
        filterYear,
        setFilterYear,
        filterMounth,
        setFilterMounth,
        filterStore,
        setFilterStore,
      }}
    >
      {children}
    </FiltersContext.Provider>
  );
};

export const useFilters = () => useContext(FiltersContext);
