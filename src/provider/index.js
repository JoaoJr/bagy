// import DataProvider from "./dataProvider";
import { MockDataProvider } from "./MockDataProvider";
import { FiltersProvider } from "./filtersProvider";

const Providers = ({ children }) => {
  return (
    <MockDataProvider>
      <FiltersProvider>{children}</FiltersProvider>
    </MockDataProvider>
  );
};

export default Providers;
