import { createContext, useContext, useState } from "react";
import { data } from "../../Mock";

export const MockDataContext = createContext();

export const MockDataProvider = ({ children }) => {
  const [mockData, setMockData] = useState(data);

  return (
    <MockDataContext.Provider value={{ mockData, setMockData }}>
      {children}
    </MockDataContext.Provider>
  );
};

export const useMockData = () => useContext(MockDataContext);
