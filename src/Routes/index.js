import { Routes, Route } from "react-router-dom";
import OverviewPage from "../Pages/overviewPage";
const RoutesPages = () => {
  return (
    <>
      <Routes>
        <Route path="overview" element={<OverviewPage />} />
      </Routes>
    </>
  );
};

export default RoutesPages;
