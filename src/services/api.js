import axios from "axios";

const api = axios.create({
  baseURL: "http://localhost:3004/data/",
});
export default api;
