import { ContainerStyled } from "./styles";
import Menu from "../../Components/menu";
import CardOverview from "../../Components/cardOverview";
import Chart from "../../Components/chart";
import SalesReport from "../../Components/salesReport";
import TaskReport from "../../Components/taskReport";

import { useMockData } from "../../provider/MockDataProvider";

const OverviewPage = () => {
  const { mockData } = useMockData();

  return (
    <ContainerStyled>
      <Menu title={"Visão Geral"} />
      <section className="FeaturedInfoStyled">
        <div className="Card-total">
          <h3>Total de Lojas</h3>
          <div>{mockData.totalStores}</div>
        </div>
        <CardOverview
          title={"Faturamento total"}
          content={mockData.totalBilling}
        />
        <CardOverview
          title={"Loja destaque"}
          content={mockData.featuredStore}
        />
        <CardOverview title={"Meta Mensal"} content={mockData.monthlyGoal} />
      </section>
      <section className="Box-chart">
        <Chart />
      </section>

      <section className="Box-tickets-tasks">
        <div className="Box-tickets">
          <div className="Content">
            <SalesReport />
          </div>
        </div>
        <div className="Box-tasks">
          <div className="Content">
            <TaskReport />
          </div>
        </div>
      </section>
    </ContainerStyled>
  );
};
export default OverviewPage;
