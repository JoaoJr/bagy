import styled from "styled-components";

export const ContainerStyled = styled.div`
  width: 1183px;
  height: 1243px;
  background: #fff;

  .FeaturedInfoStyled {
    width: 100%;
    display: flex;
    justify-content: space-evenly;
    align-items: center;
    margin: 20px 0px;
  }

  .Card-total {
    width: 258px;
    height: 134px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    font-family: "Mulish";

    h3 {
      color: var(--gray);
      font-size: 18px;
      margin-bottom: 10px;
    }

    div {
      font-size: 30px;
      font-weight: 800;
      text-shadow: 1px 5px 4px gray;
    }
  }

  .Box-chart {
    width: 1122px;
    height: 546px;
    margin: 0 auto;

    border: solid 1px var(--gray);
    border-radius: 10px;
    font-family: "Mulish";
  }

  .Box-tickets-tasks {
    display: flex;
    justify-content: space-evenly;
    margin-top: 30px;
  }

  .Box-tickets {
    width: 546px;
    height: 336px;
    border: solid 1px var(--gray);
    border-radius: 10px;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  }

  .Content {
    margin: 10px 5px 10px 0px;
    height: 95%;
    overflow: auto;

    ::-webkit-scrollbar {
      width: 10px;
    }

    ::-webkit-scrollbar-track {
      border-radius: 10px;
      background-color: rgba(196, 196, 196, 0.5);
    }

    ::-webkit-scrollbar-thumb {
      border-radius: 10px;
      background-color: var(--pink);
    }
  }

  .Box-tasks {
    width: 546px;
    height: 336px;
    border: solid 1px var(--gray);
    border-radius: 10px;
  }
`;
