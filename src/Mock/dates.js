export const months = [
  { month: "Janeiro" },
  { month: "Fevereiro" },
  { month: "Março" },
  { month: "Abril" },
  { month: "Maio" },
  { month: "Junho" },
  { month: "Julho" },
  { month: "Agosto" },
  { month: "Setembro" },
  { month: "Outubro" },
  { month: "Novembro" },
  { month: "Dezembro" },
];

export const years = [{ year: 2022 }, { year: 2021 }, { year: 2020 }];

export const stores = [
  { store: "Estilo Pri" },
  { store: "Estilo Duda" },
  { store: "Estilo Ju" },
];
