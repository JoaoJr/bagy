import { ContainerStyled } from "./styles";
import formatValue from "../../utils/formatValue";

import { useMockData } from "../../provider/MockDataProvider";

const CardProducts = () => {
  const { mockData } = useMockData();

  const detailsPurchase = mockData.users.map((item) => {
    return item.purchaseDetails;
  });

  console.log(detailsPurchase);

  return (
    <ContainerStyled>
      <ul className="Task-ul-details">
        {mockData.users.map((item, index) => {
          return (
            <div key={index}>
              {item.purchaseDetails.map((details, i) => {
                return (
                  <li key={i}>
                    <div className="Task-details-purchase">
                      <div className="Task-details-product-store">
                        {details.name} #{details.id}
                      </div>
                      <div className="Task-details-product-store">
                        {details.storeName}
                      </div>
                      <div className="Task-details-product-price">
                        {formatValue(details.price)}
                      </div>
                      <div className="Task-details-product-purchaseData">
                        {details.purchaseData}
                      </div>
                    </div>
                  </li>
                );
              })}
            </div>
          );
        })}
      </ul>
    </ContainerStyled>
  );
};

export default CardProducts;

// <ul className="Task-ul-details">
//   {mockData.map((item, index) => {
//     return (
//       <li key={index}>
//         {item.users.map((user, i) => {
//           return (
//             <div key={i}>
//               {user.purchaseDetails.map((p, indexP) => {
//                 return (
//                   <div key={indexP}>
//                     <div className="Task-details-purchase">
//                       <div className="Task-details-product-store">
//                         {p.name} #{p.id}
//                       </div>
//                       <div className="Task-details-product-store">
//                         {user.store}
//                       </div>
//                       <div className="Task-details-product-price">
//                         {formatValue(p.price)}
//                       </div>
//                       <div className="Task-details-product-purchaseData">
//                         {p.purchaseData}
//                       </div>
//                     </div>
//                   </div>
//                 );
//               })}
//             </div>
//           );
//         })}
//       </li>
//     );
//   })}
// </ul>;
