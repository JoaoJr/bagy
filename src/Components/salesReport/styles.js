import styled from "styled-components";

export const ContainerStyled = styled.div`
  .Box-general-value {
    display: flex;
    align-items: center;
    justify-content: space-between;
    font-family: "Mulish";

    select {
      margin: 0px 10px;
      border: none;
      background-color: transparent;
      text-decoration: underline;
      font-size: 18px;
      color: var(--pink);
    }
  }

  .General-value {
    display: flex;
    flex-direction: column;
    margin: 37px 32px 13px 32px;

    h3 {
      margin-bottom: 10px;
    }

    div {
      color: var(--blue);
      font-size: 14px;
      font-weight: bold;
    }
  }

  .ul-details {
  }

  li {
    display: flex;
    justify-content: space-evenly;
    align-items: center;
    border-bottom: 1px solid var(--gray);
    height: 58px;

    p {
      font-size: 14px;
    }

    .p-store {
    }
    .p-purchases,
    .p-total {
      color: var(--gray);
    }
  }
`;
