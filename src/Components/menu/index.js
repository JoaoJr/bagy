import { ContainerStyled } from "./styles";
import { useState } from "react";
import SearchIcon from "../../assets/img/search.svg";
import NotificationIcon from "../../assets/img/bell.svg";
import { useMockData } from "../../provider/MockDataProvider";

const Menu = ({ title }) => {
  const { mockData } = useMockData();
  const [isShown, setIsShown] = useState(false);

  const username = mockData.users[0].name;

  const handleShow = () => {
    setIsShown(!isShown);
  };
  return (
    <ContainerStyled>
      <div className="title-page">
        <h2>{title}</h2>
      </div>
      <div className="menu-nav">
        {isShown && <input />}
        <button onClick={() => handleShow()}>
          <img src={SearchIcon} alt="SearchIcon" />
        </button>
        <img
          className="icon-notification"
          src={NotificationIcon}
          alt="NotificationIcon"
        />
        <div className="line-vertical" />
        <h4>
          {username.length > 15 ? `${username.substring(0, 15)}...` : username}
        </h4>
        <figure>
          <img
            className="img-profile"
            src={"https://picsum.photos/200"}
            alt="imgProfile"
          />
        </figure>
      </div>
    </ContainerStyled>
  );
};
export default Menu;
