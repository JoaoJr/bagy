import styled from "styled-components";

export const ContainerStyled = styled.div`
  border-bottom: 1px solid var(--gray);
  width: 100%;
  height: 20%;
  font-family: "Mulish";
 

  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;

} 
  
 .info-title-month{
      font-size: 18px;
      font-weight: bold;
      color: var(--gray);
      margin: 5px;
  }


    select {
      font-weight: bold;
      text-align: center;
      border: none;
      background-color: transparent;
      text-decoration: underline;
      font-size: 18px;
    }
  }
`;
