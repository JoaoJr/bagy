import { ContainerStyled } from "./styles";
import { useFilters } from "../../provider/filtersProvider";

const CardFilterMonth = ({ data, keyData, title }) => {
  const { setFilterMounth } = useFilters();

  return (
    <ContainerStyled>
      <div className="info-title-month">{title}</div>
      <select onChange={(e) => setFilterMounth(e.target.value)}>
        {data.map((data, index) => (
          <option key={index} value={data.month}>
            {data.month}
          </option>
        ))}
      </select>
    </ContainerStyled>
  );
};

export default CardFilterMonth;
