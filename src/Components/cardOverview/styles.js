import styled from "styled-components";

export const CardStyled = styled.div`
  width: 258px;
  height: 134px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  border: solid 1px var(--gray);
  border-radius: 10px;
  font-family: "Mulish";

  h3 {
    color: var(--gray);
    font-size: 18px;
    margin-bottom: 10px;
  }

  div {
    font-size: 30px;
    font-weight: bold;
  }

  :hover {
    color: var(--pink);
    border: solid 2px var(--gray);
    cursor: pointer;

    h3 {
      color: var(--pink);
    }
  }
`;
