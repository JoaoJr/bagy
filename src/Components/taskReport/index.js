import { ContainerStyled } from "./styles";
import CardProducts from "../CardProducts";

const TaskReport = () => {
  return (
    <ContainerStyled>
      <div className="Task-title">
        <h3>Produto</h3>
        <h3>Loja</h3>
        <h3>Preço</h3>
        <h3>Data</h3>
      </div>
      <CardProducts />
    </ContainerStyled>
  );
};

export default TaskReport;
